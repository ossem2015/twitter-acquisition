-# TWITTERMUNCH #

-

-Steps for spinning up new twittermunch ingest instance:

-

-### Setup Steps ###

-

-* wget https://storage.googleapis.com/golang/go1.5.1.linux-amd64.tar.gz

-* sudo tar -C /usr/local -xzf go1.5.1.linux-amd64.tar.gz

-* sudo apt-get install git

-* sudo apt-get install mercurial

-* sudo echo export 'GOPATH=$HOME/go' >> /etc/profile

-* sudo echo export 'PATH=$PATH:/usr/local/go/bin' >> /etc/profile

-* mkdir -p $HOME/go/src $HOME/go/lib $HOME/go/bin $HOME/go/pkg

-* cd $HOME/go/src; git clone https://<your account>@bitbucket.org/ossem2015/twitter-acquisition.git

-* cd twitter-aquisition

-* go get ./...

-* go build application.go

-

-### How do I get set up? ###

-Roll an EC2 instance and set up using the steps above. Remember to change the <your account> part to your bitbucket account name

-

-### Contribution guidelines ###

-

-* Writing tests

-* Code review

-* Other guidelines

-

-### Who do I talk to? ###

-

-* Repo owner or admin

-* Other community or team contact