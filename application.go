//Ossam
//Keith Baton
//application.go
package main

import (
	"math/rand"
	"strconv"
	"sync"
	"bufio"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"code.google.com/p/go.net/websocket"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/firehose"
	"github.com/darkhelmet/twitterstream"
)

type Tweet struct {
	Id			string	`json:"id"`
	Hashtags    []string `json:"hashtags"`
	Mentions    []string `json:"mentions"`
	Retweet     *Tweet
	Retweeted   bool
	Text        string             `json:"text"`
	User        twitterstream.User `json:"user"`
	Name        string             `json:"name"`
	ScreenName  string             `json:"screenName"`
	Coordinates []float64          `json:"coordinates"`
	CreatedAt   twitterstream.Time `json:"created_at"`
}

type Client struct {
	conn      *websocket.Conn
	closeConn chan bool
	closed    bool
}

var (
	// keywords       = flag.String("keywords", "", "keywords to track")
	wait           = 1
	maxWait        = 600 // Seconds
	consumerKey    string
	consumerSecret string
	accessToken    string
	accessSecret   string
	firehoseStream string
	s3             string
	awsRegion      string
	awsKey         string
	awsSecret      string
	port           string
	svc            *firehose.Firehose
	tweets         = make(chan *Tweet, 5000)
	clients        = make(chan *Client)
	random		   = rand.New(rand.NewSource(time.Now().UnixNano()))
	count          = 0
	hashtag_history = struct {
		sync.RWMutex
		m map[string]bool
	}{m: make(map[string]bool)}
	
	deliveryStream = aws.String("TwitterStream")
	destinationId  = aws.String("someknowntwitterbucket")
)

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

// readLines reads a whole file into memory
// and returns a slice of its lines.
func readLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}
func init() {
	lines, err := readLines("config.ini") // For read access.
	if err != nil {
		log.Fatalf("Error reading config file: %v", err.Error())
	}
	f, err := os.OpenFile("writeCount.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening log file: %v\n", err)
	}
	log.SetOutput(f)
	consumerKey = strings.SplitN(lines[1], ":= ", 2)[1]
	consumerSecret = strings.SplitN(lines[2], ":= ", 2)[1]
	accessToken = strings.SplitN(lines[3], ":= ", 2)[1]
	accessSecret = strings.SplitN(lines[4], ":= ", 2)[1]
	firehoseStream = strings.SplitN(lines[5], ":= ", 2)[1]
	s3 = strings.SplitN(lines[6], ":= ", 2)[1]
	awsRegion = strings.SplitN(lines[7], ":= ", 2)[1]
	awsKey = strings.SplitN(lines[8], ":= ", 2)[1]
	awsSecret = strings.SplitN(lines[9], ":= ", 2)[1]
	port = strings.SplitN(lines[10], ":= ", 2)[1]
	strings.Trim(consumerKey, " \n\r\t")
	strings.Trim(consumerSecret, " \n\r\t")
	strings.Trim(accessToken, " \n\r\t")
	strings.Trim(accessSecret, " \n\r\t")
	strings.Trim(firehoseStream, " \n\r\t")
	strings.Trim(s3, " \n\r\t")
	strings.Trim(awsRegion, " \n\r\t")
	strings.Trim(awsKey, " \n\r\t")
	strings.Trim(awsSecret, " \n\r\t")
	strings.Trim(port, " \n\r\t")
	svc = firehose.New(session.New(), &aws.Config{Credentials: credentials.NewStaticCredentials(awsKey, awsSecret, ""), Region: aws.String(awsRegion)})
	params := &firehose.DescribeDeliveryStreamInput{
		DeliveryStreamName:          aws.String(firehoseStream), // Required
		ExclusiveStartDestinationId: aws.String(s3),
		Limit: aws.Int64(1),
	}
	resp, err := svc.DescribeDeliveryStream(params)
	if err != nil {
		log.Fatalf("Firehose stream not ready: %v", err.Error())
	}
	log.Printf("Firehose Ready\nDetails: %v", resp)
	if consumerKey == "" || consumerSecret == "" {
		log.Fatalln("consumer tokens left blank")
	}

	if accessToken == "" || accessSecret == "" {
		log.Fatalln("access tokens left blank")
	}
	if port == "" {
		port = "3000"
	}
	initBearer()
	log.Println(time.Now())
	
	go wsConnHandler()
	go downloader()
}

func translateTweet(currTweet *twitterstream.Tweet) *Tweet {
	if currTweet == nil {
		return nil
	}
	tweet := new(Tweet)
	
	for _, hashtag := range currTweet.Entities.Hashtags {
		tweet.Hashtags = append(tweet.Hashtags, hashtag.Text)
	}
	for _, mention := range currTweet.Entities.Mentions {
		tweet.Mentions = append(tweet.Mentions, mention.ScreenName)
	}
	tweet.Text = currTweet.Text
	tweet.Name = currTweet.User.Name
	tweet.User = currTweet.User
	tweet.ScreenName = currTweet.User.ScreenName
	if currTweet.Coordinates != nil {
		tweet.Coordinates = append(tweet.Coordinates, currTweet.Coordinates.Long.Float64())
		tweet.Coordinates = append(tweet.Coordinates, currTweet.Coordinates.Lat.Float64())
	}
	tweet.CreatedAt = currTweet.CreatedAt
	tweet.Id = tweet.CreatedAt.String() +"-"+ strconv.FormatInt(random.Int63(), 10)
	tweet.Retweeted = currTweet.Retweeted
	tweet.Retweet = translateTweet(currTweet.RetweetedStatus)

	return tweet
}
func decodeTweets(conn *twitterstream.Connection, waitVal int) {
	var tweetBuffer chan *twitterstream.Tweet = make(chan *twitterstream.Tweet, 5000)
	var mentioned = make(chan *twitterstream.Tweet, 5000)
	var exitDecode chan bool = make(chan bool, 1)
	//buffer tweets

	go func() {
		defer close(tweetBuffer)
		defer func() {
			if r := recover(); r != nil {
				log.Printf("Recovered in reciving pipeline: %s", r)
				exitDecode <- true
			}
		}()
		for {
			if retrievedTweet, err := conn.Next(); err == nil {
				tweetBuffer <- retrievedTweet
			} else {
				//log.Printf("decoding tweet failed: %s", err.Error())
				break
			}
		}
		exitDecode <- true
	}()
	go func() {
		defer func() {
			if r := recover(); r != nil {
				log.Printf("Recovered in tweets pipeline: %s", r)
				exitDecode <- true
			}
		}()
		for currTweet, ok := <-tweetBuffer; ok; currTweet,ok = <-tweetBuffer {
			tweets <- translateTweet(currTweet)
		}
		exitDecode <- true
	}()
	go func() {
		defer func() {
			if r := recover(); r != nil {
				log.Printf("Recovered in hashtag-parse pipeline: %s", r)
				exitDecode <- true
			}
		}()
		for currTweet, ok := <-mentioned; ok; currTweet,ok = <-mentioned {
			tweets <- translateTweet(currTweet)
		}
		exitDecode <-true
	}()
	go func() {
		defer func() {
			if r := recover(); r != nil {
				//log.Printf("Recovered in waiting hashtag pipeline: %s", r)
				exitDecode <- true
			}
		}()
		for waitingTag, ok := <-waitingTags; ok; waitingTag = <- waitingTags {
			var counterVal int
			if counterVal = inc(); counterVal >= 450 {
				resetCounter()
				hashtag_history.RLock()
				hashtag_history.m = make(map[string]bool)
				hashtag_history.RUnlock()	
				log.Println("Hashtag Parser - sleeping for 15 minutes to reset requests.")
				time.Sleep(15 * time.Minute)
				log.Println("Hashtag Parser - resuming")
				log.Println("Hashtag Parser - resumed")
			} else {
				go func() {
					getHashtagTweets(waitingTag,-1, mentioned)
				}()
			}
		}
	}()
	go func() {
		for{
			waitingTags <- "a"
			time.Sleep(30 * time.Second)
		}
	}()
	<-exitDecode
}

func downloader() {
	client := twitterstream.NewClient(consumerKey, consumerSecret, accessToken, accessSecret)
	for {
		//log.Printf("tracking keywords %s", *keywords)
		conn, err := client.Sample()

		defer conn.Close()
		if err != nil {
			log.Printf("tracking failed: %s", err)
			wait = wait << 1
			log.Printf("waiting for %d seconds before reconnect", min(wait, maxWait))
			time.Sleep(time.Duration(min(wait, maxWait)) * time.Second)
			continue
		} else {
			wait = 1
		}
		decodeTweets(conn, wait)
	}
}

func wsConnHandler() {
	conns := make(map[*websocket.Conn]*Client)
	removeList := make(chan *websocket.Conn)
	//launch background thread to remove clients
	go func() {
		v := <-removeList
		delete(conns, v)
		log.Println("Cleaned up Closed Connection")
	}()
	for {
		select {
		case client := <-clients:
			conns[client.conn] = client
		case tweet := <-tweets:
			json_tweet, _ := json.Marshal(tweet)

			params := &firehose.PutRecordInput{
				DeliveryStreamName: deliveryStream, // Required
				Record: &firehose.Record{ // Required
					Data: append(json_tweet, []byte("\n\r")...), // Required
				},
			}
			for conn, client := range conns {
				if !client.closed {
					json_tweet, _ := json.Marshal(tweet)
					if _, err := conn.Write(json_tweet); err != nil {
						conn.Close()
						client.closeConn <- true
						close(client.closeConn)
						client.closed = true
					}
				} else {
					removeList <- conn
				}
			}
			svc.PutRecord(params)
			if count++; count%1000 == 0 {
				log.Printf("Count: %v at %v", count, time.Now())
			}
		}
	}
}

func IndexHandler(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Content-Type", "text/html")
	file, err := ioutil.ReadFile("index.html")
	if err != nil {
		panic(err)
	}
	res.Write(file)
}

func TweetsHandler(ws *websocket.Conn) {
	log.Println("Client Connected")
	client := &Client{ws, make(chan bool), false}
	clients <- client
	<-client.closeConn
}

func main() {
	server := http.NewServeMux()

	server.HandleFunc("/", IndexHandler)
	server.Handle("/ws", websocket.Handler(TweetsHandler))

	err := http.ListenAndServe(":"+port, server)
	if err != nil {
		panic(err)
	}

}
