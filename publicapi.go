// publicapi
package main

import (
	"io"
	"strconv"
	"encoding/base64"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
	"github.com/darkhelmet/twitterstream"
)

var AUTHURL string = "https://api.twitter.com/oauth2/token"
var bearer_token Bearer
var waitingTags = make(chan string)
var requestCount = make(chan int)
var inc = counter()

type Bearer struct {
	Token_type   string
	Access_token string
}

type HastagResults struct {
	Statuses []*twitterstream.Tweet `json:statuses`
}

func initBearer() {
	if bearer_token.Access_token == "" {
		urlEncodeKey := url.QueryEscape(consumerKey)
		urlEncodeSecret := url.QueryEscape(consumerSecret)
		concatenatedKey := urlEncodeKey + ":" + urlEncodeSecret
		data := []byte(concatenatedKey)
		encoded := base64.StdEncoding.EncodeToString(data)
		err := json.Unmarshal([]byte(urlPostOauthRequest(encoded)), &bearer_token)
		if err != nil {
			panic(err)
		}
		log.Println("Initialized Search API")
	}
}

func urlPostOauthRequest(keyenc string) string {
	hc := http.Client{}
	req, err := http.NewRequest("POST", AUTHURL, strings.NewReader("grant_type=client_credentials"))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
	req.Header.Add("Authorization", "Basic "+keyenc)
	resp, err := hc.Do(req)
	if err != nil {
		log.Printf("%s", err)
	} else {
		defer resp.Body.Close()
		contents, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Printf("%s", err)
		} else {
			return string(contents)
		}
	}
	return ""
}

func getHashtagUri(hashtag string, last_id int64) string {
	baseUri := "https://api.twitter.com/1.1/search/tweets.json?q=a"
	baseUri += "&src=typd"
	if last_id != -1 {
		baseUri += "&since_id=" + strconv.FormatInt(last_id,10)
	}
	return  baseUri + "&count=100"
}

func counter() (func() int) {
	count := 0
	return func() int {
		count++
		return count
	}
}

func resetCounter() {
	inc = counter()
}

func getHashtagTweets(hashtag string, last_id int64, tagChannel chan<- *twitterstream.Tweet) {
	if bearer_token.Access_token == "" {
		return
	}
	uri := getHashtagUri(hashtag, last_id)
	client := &http.Client{}
	req, err := http.NewRequest("GET", uri, nil)
	if err != nil {
		log.Printf("%s", err)
	} else {
		req.Header.Set("Authorization", "Bearer "+bearer_token.Access_token)
		res, err := client.Do(req)
		if err != nil {
			log.Printf("%s", err)
		} else {
			defer res.Body.Close()
			
			decoder := json.NewDecoder(res.Body)
			
			var tweets = new(HastagResults)
			if err := decoder.Decode(tweets); err == io.EOF {
           		return
	        } else if err != nil {
	            log.Printf("Failed parsing related hashtag: %s\n\tcause:%s",hashtag, err)
	        }
			for _,tweet := range tweets.Statuses {
				tagChannel <- tweet
			}
			if len(tweets.Statuses) >= 100 {
				latest := tweets.Statuses[99].Id
				//add close before recurse here because
				//	otherwise it would hold the resource open
				//	through the whole iteration
				res.Body.Close()
				getHashtagTweets(hashtag,latest,tagChannel)
				
			}
		}
	}
}
